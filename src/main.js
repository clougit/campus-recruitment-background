import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import router from './router'
import store from './store'
import { $post, $get, $put, $delete } from "@/utils/api.js"
import { downloadReq } from "@/utils/download.js"
import VueCookies from 'vue-cookies'
import { initMenu } from './utils/menu'
import { Message } from 'element-ui'

Vue.config.productionTip = false

Vue.use(ElementUI)
Vue.use(VueCookies)

Vue.prototype.$post = $post
Vue.prototype.$get = $get
Vue.prototype.$put = $put
Vue.prototype.$delete = $delete
Vue.prototype.$downloadReq = downloadReq

// 全局前置守卫，判断用户是否登陆以及记住我
/* router.beforeEach((to, from, next) => {
  let token = Vue.$cookies.get('Authorization')
  if (to.path != '/login' && to.path != '/') {
    if (token !== null && token !== '') {
      initMenu(router, store)
      if (!sessionStorage.getItem('user')) {
        $get("admin/detail").then((res) => {
          if (res.code == 200 && res.data) {
            sessionStorage.setItem("user", JSON.stringify(res.data));
            next()
          }
        });
      } else {
        next()
      }
      next()
    } else {
      next('/login')
    }
  } else {
    next()
  }
}) */

router.beforeEach((to, from, next) => {
  if (to.path === '/login' || to.path === '/') {
    next()
  } else {
    let token = sessionStorage.getItem('tokenStr')
    if (token === null || token === '') {
      Message.warning("请先登录")
      next('/login')
    } else {
      initMenu(router, store)
      if (!sessionStorage.getItem('user')) {
        $get('admin/detail').then((res) => {
          if (res.code == 200 && res.data) {
            sessionStorage.setItem('user', JSON.stringify(res.data))
            next()
          }
        })
      } else {
        next()
      }
    }
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
